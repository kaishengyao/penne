"""
Deep recurrent language model. This is similar to rnnlm1.py, but uses minibatches.

python rnnlm3.py

"""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
import numpy
import random
import itertools
import os.path

hidden_dims = 100
depth = 1

#train = lm.read_data("../data/inferno.txt")
#train = lm.read_data("../data/ptb.train.txt")
train = lm.read_data("d:/data/ptbdata/ptb.trn")
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

#valid = lm.read_data("../data/purgatorio.txt")
valid = lm.read_data("d:/data/ptbdata/ptb.dev")
testset = lm.read_data("d:/data/ptbdata/ptb.tst")

#model_dir = "d:/exp/rnn/lstmlm/ptbd"
#model_fn  = 'rnnlm3'

batch_size = 256
num_parallel = 128

layers = [recurrent.LSTM(hidden_dims, -len(vocab), hidden_dims)]
for i in xrange(depth-1):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims, hidden_dims))
layers.append(recurrent.Map(make_layer(hidden_dims, len(vocab), f=logsoftmax)))
rnn = recurrent.Stacked(*layers)

def make_network(batch):
    correct = [map(numberizer.numberize, words) for words in batch]
    input = [[numberizer.numberize("<s>")] + nums[:-1] for nums in correct]
    output = rnn.transduce_batch(input, num_parallel)
    loss = constant(0.)
    for i in xrange(len(output)):
        for j in xrange(len(output[i])):
            o = output[i][j]
            loss -= o[correct[i][j]]
    return loss

def eval_ppl(data, batch_size):
    this_loss = 0.
    dsize = 0
    for batch in lm.batches(data, batch_size):
        loss = make_network(batch)
        this_loss += compute_value(loss)
        dsize += sum(len(words) for words in batch)
    ppl = numpy.exp(this_loss/dsize)
    return ppl

# previous epoch validation set ppl
prev_valid_ppl = numpy.infty
learning_rate = 0.1

trainer = Adagrad(learning_rate=learning_rate)

for epoch in xrange(50):
    random.shuffle(train)
#    fn = model_dir + '/' + model_fn + '.epoch' + str(epoch)
#    if os.path.exists(fn) == True:
#        load_model(fn, rnn)
#        continue

    train_loss = 0.
    train_size = 0
    for batch in lm.batches(train, batch_size):
        loss = make_network(batch)
        train_loss += trainer.receive(loss)
        train_size += sum(len(words) for words in batch)
    train_ppl = numpy.exp(train_loss/train_size)

    valid_ppl = eval_ppl(valid, batch_size)
    test_ppl = eval_ppl(testset, batch_size)

    print "epoch=%s train=%s valid=%s test=%s" % (epoch, train_ppl, valid_ppl, test_ppl)

    # monitor learning rate
    if valid_ppl >= prev_valid_ppl:
        learning_rate *= 0.5
        trainer.learning_rate = learning_rate
    prev_valid_ppl = valid_ppl

    '''
        fn = model_dir + '/' + model_fn + '.epoch' + str(epoch-1)
        load_model(fn, rnn)

        fn = model_dir + '/' + model_fn + '.epoch' + str(epoch)
        save_model(fn, tra)
    else:
        save_model(fn, rnn.model)
    '''

